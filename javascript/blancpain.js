$(document).ready(function(){
    $(".default-1 img").hover(function() {
		$("img").addClass( "bw" );
		$(this).removeClass( "bw" );
	  }, function() {
		$("img").removeClass( "bw" );
	  }
    );
	$(".default-2 img").hover(function() {
		$("img").addClass( "bw" );
		$(this).removeClass( "bw" );
	  }, function() {
		$("img").removeClass( "bw" );
	  }
    );
	$(".default-3 img").hover(function() {
		$("img").addClass( "bw" );
		$(this).removeClass( "bw" );
	  }, function() {
		$("img").removeClass( "bw" );
	  }
    );
	$(".default-4 img").hover(function() {
		$("img").addClass( "bw" );
		$(this).removeClass( "bw" );
	  }, function() {
		$("img").removeClass( "bw" );
	  }
    );
	$(".default-5 img").hover(function() {
		$("img").addClass( "bw" );
		$(this).removeClass( "bw" );
	  }, function() {
		$("img").removeClass( "bw" );
	  }
    );
	$(".default-6 img").hover(function() {
		$("img").addClass( "bw" );
		$(this).removeClass( "bw" );
	  }, function() {
		$("img").removeClass( "bw" );
	  }
    );
	
	/*
    $(".home-button").click(function(){
		$(this).parent("div").parent("div").animate({ "margin-right": -99999 },"slow");
		$(this).parent("div").parent("div").css("display", "none");
		$(".main-body").css("display", "block");
		$(".main-body").animate({ "margin-left": 0 },"slow");
    });
	$(".default-3").click(function(){
		$(".main-body").animate({ "margin-left": -99999 },"slow");
		$(".main-body").css("display", "none");
		$(".click-3").animate({ "margin-right": 0 },"slow");
		$(".click-3").css("display", "block");
		
    });
	*/
    
    $(".home-button").click(function(){
        $(".main-body").css("display", "block"),
        $(".click-1").css("display", "none"),
        $(".click-2").css("display", "none"),
        $(".click-3").css("display", "none"),
        $(".click-6").css("display", "none");
		$("body").css("overflow", "hidden");
    });
    $(".default-1").click(function(){
        $(".main-body").css("display", "none"),
        $(".click-1").css("display", "block");
		$("body").css("overflow-y", "auto");
    });
    $(".default-2").click(function(){
        $(".main-body").css("display", "none"),
        $(".click-2").css("display", "block");
		$("body").css("overflow-y", "scroll");
    });
    $(".default-3").click(function(){
        $(".main-body").css("display", "none"),
        $(".click-3").css("display", "block");
		$("body").css("overflow-y", "scroll");
    });
    $(".default-6").click(function(){
        $(".click-6").css("display", "block");
		$("body").css("overflow-y", "scroll");
    });

});
